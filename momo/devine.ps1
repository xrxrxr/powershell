﻿]write-Output "******************************" "*  Plus ou moins en PScript  *" "******************************" ""

write-Output "Le nombre Mystère est compris entre 0 et 100" #on affiche la présentation
$variable=Read-Host "Quel est le nombre mystère ?" #on demande à l'utilisateur d'entre un chiffre
$Random=Get-Random -minimum 0 -maximum 100 #on tire au sort un nombre
$tentative=1
While ($variable -ne $random) { #tant que l'utilisateur n'as pas trouvé
if ($variable -lt $Random) {Write-Output "C'est plus!"} #si le nombre est plus petit
if ($variable -gt $Random) {Write-Output "C'est moins"} #si le nombre est plus grand
$variable=Read-Host "Quel est le nombre mystère ?" #on demande à l'utilisateur d'entre un chiffre
$tentative++
}
Write-OutPut ""
Write-Output "Vous avez gagné, le nombre mistère était bien:" $Random "En $tentative coups !"
Read-Host