﻿ #Guess the number

[int] $Number = (Get-Random 100) + 1
[int] $Guess = 0

Write-Host "pense a un nombre entre  1 et 100."

While ($Number -ne $Guess) {

    Write-Host -NoNewline "devine le nombre? "
    $Guess = [int] (Read-Host)

    If ($Guess -gt $Number) { Write-Host "$Guess trop grand." }
    If ($Guess -lt $Number) { Write-Host "$Guess trop petit." }   

}
Write-Host "Correct! $Number c'est le nombre que je pensais!"